package logger

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/kataras/iris"
	"github.com/kataras/iris/middleware/logger"

	"github.com/ryanuber/columnize"
)

var excludeExtensions = [...]string{
	".js",
	".css",
	".jpg",
	".png",
	".ico",
	".svg",
}

func IrisLogger(logFile *os.File) (h iris.Handler) {

	c := logger.Config{
		Status:  true,
		IP:      true,
		Method:  true,
		Path:    true,
		Columns: true,
	}

	c.LogFunc = func(now time.Time, latency time.Duration, status, ip, method, path string, message interface{}, headerMessage interface{}) {
		// output := logger.Columnize(now.Format("2006/01/02 - 15:04:05"), latency, status, ip, method, path, message, headerMessage)
		line := fmt.Sprintf("Time=%s | Status=%v | Latency=%4v | IP=%s | Method=%s | Path=%s",
			now.Format("2006/01/02 - 15:04:05"), status, latency, ip, method, path)
		if message != nil {
			line += fmt.Sprintf(" | HeaderMessage=%v", headerMessage)
		}

		outputC := []string{line}

		output := columnize.SimpleFormat(outputC)

		Info.Println(output)
	}

	//	we don't want to use the logger
	// to log requests to assets and etc
	c.AddSkipper(func(ctx iris.Context) bool {
		path := ctx.Path()
		for _, ext := range excludeExtensions {
			if strings.HasSuffix(path, ext) {
				return true
			}
		}
		return false
	})

	h = logger.New(c)

	return
}
